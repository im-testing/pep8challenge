# PEP 8 Challenge

[![Maintainability](https://api.codeclimate.com/v1/badges/112f63fc87acf4790d75/maintainability)](https://codeclimate.com/github/ryan-ccn/pep8challenge/maintainability)

This is a fun challenge project created by [Ryan Cao](https://github.com/ryan-ccn) and is licensed under the MIT license.

It challenges the Python standard library itself with [PEP 8](https://pep8.org/). This project is **only for fun** and not to humiliate or anything. Though it *might* be humiliating.

### Usage

If you are a person of excellent taste:
```bash
$ pipenv run main.py
```
If you are not (tsk tsk!):
```bash
$ pip install -r requirements.txt
```

### Important note

All of the Python programs in this project comply with PEP 8 completely.