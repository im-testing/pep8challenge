import subprocess
import requests
import shutil
import glob
import os

# Change working directory to project root
print('Changing working directory to project root...')

os.chdir(os.path.abspath(os.path.join(
    os.path.normpath(__file__), os.path.pardir)))

# Download Python GitHub repo contents
print('Downloading Python GitHub repo contents...')

url = 'https://github.com/python/cpython/archive/master.zip'
r = requests.get(url)
open('python_files.zip', 'wb').write(r.content)
shutil.unpack_archive('python_files.zip', extract_dir='python_files')

# Create PEP 8-compliant Python folder
print('Creating PEP 8-compliant Python folders...')

shutil.copy('python_files', 'pep8_python_files')

# Retrieve all Python files
print('Retrieving all Python files...')

all_python_files = glob.glob('pep8_python_files/**/**.py', recursive=True)

print(len(all_python_files), 'Python files in Python standard library to be checked\n')

for python_file in all_python_files:
    print(python_file + '\n')

    print('Before autopep8:\n')
    subprocess.call(['pycodestyle', python_file])
    print()

    print('After autopep8:\n')
    # Write the formatted things to a PEP 8-compliant file
    subprocess.call(['autopep8', python_file, '--in-place', '--aggressive', '--aggressive'])
    print()